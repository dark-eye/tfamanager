/*
 * Copyright (C) 2022 Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Joan CiberSheep <cibersheep@gmail.com>
 */
import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: donateDialog

    property real runs

    title: i18n.tr("Consider donating")
    text: i18n.tr("You have been using this app %1 times.").arg(runs)

    Button {
        text: i18n.tr("Donate")
        color: theme.palette.normal.positive

        onClicked: {
            Qt.openUrlExternally('https://liberapay.com/cibersheep/donate')
            PopupUtils.close(donateDialog)
        }
    }
    
    Button {
        text: i18n.tr("Close")

        onClicked: PopupUtils.close(donateDialog)
    }
}
