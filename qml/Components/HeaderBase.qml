/*
 */

import QtQuick 2.9
import Lomiri.Components 1.3

PageHeader {
    StyleHints {
        foregroundColor: root.titleColor
        backgroundColor: root.mainColor
    }
}
